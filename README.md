# Cloud User Interface Benchmarks

Comparison of speed and usability of various cloud user interfaces

See OSF project for more information: <https://osf.io/pdgmh>